<?php
/**
 * Created by PhpStorm.
 * User: Awik.Ru
 * Date: 31.10.2014
 * Time: 19:01
 */
define("DOCUMENT_ROOT", $_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR);
?><!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="style.css">
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
        <script type="text/javascript" src="script.js"></script>
        <title>UrlChecker</title>
    </head>
    <body><?
        ini_set('display_errors', true);
        require_once (DOCUMENT_ROOT."urlchecker.php");
        require_once (DOCUMENT_ROOT."check.php");
        $codes = require_once (DOCUMENT_ROOT."codes.php");
        $uChecker = new UrlChecker(DOCUMENT_ROOT.'links.csv', 'fotoland.ru', 'fotoland.awik.ru');
        $arUrls = $uChecker->getLinks();
        $time = microtime(true);
        ?><script>self.arURLs = <?=json_encode($arUrls)?>; self.arCodes = <?=json_encode($codes)?>; </script>

        <div class="popup"><a class="close" href="#close">закрыть</a></div>
        <div class="panel">
<!--            <input type="button" value="check each" id="btnCheck">-->
<!--            <input type="button" value="check all" id="btnCheck2">-->
<!--            <input type="button" value="count" id="btnCount">-->
            <a class="btn" href="#check_each" id="btnCheck">check each</a>
            <a class="btn" href="#check_all" id="btnCheck2">check all</a>
            <a class="btn" href="#check_count" id="btnCount">count</a>
            <div class="progressBar">
                <div style="width: 0;" class="progress"></div>
                <div class="progressLabel"></div>
            </div>
<!--            <span>Число записей: </span>-->
            <span class="urlCount">
                0/<?=$uChecker->getCount()?>
            </span>
        </div>
        <div class="spacer"></div>

        <table class="resultTable">
            <tr>
                <th class="col-index">i</th>
                <th class="col-oldUrl">old url</th>
                <th class="col-oldCode">old code</th>
                <th class="col-btns"></th>
                <th class="col-newUrl">new url</th>
                <th class="col-newCode">new code</th>
                <th class="col-btns new"></th>
                <th class="col-retUrl"></th>
            </tr>
            <?
            $intCounter = 1;
            foreach($arUrls as $value)
            {
            ?>
                <tr class="line<?=$intCounter?>">
                    <td class="col-index"><?=$intCounter?></td>
                    <td class="col-oldUrl"><?=$value['old']['url']?></td>
                    <td class="col-oldCode"><?=$value['old']['code']?></td>
                    <td class="col-btns old">
                        <input type="button" class="btnUrl">
                    </td>
                    <td class="col-newUrl"><?=$value['new']['url']?></td>
                    <td class="col-newCode"></td>
                    <td class="col-btns new">
                        <input type="button" class="btnRefresh">
<!--                        <input type="button" class="btnObj">-->
                        <input type="button" class="btnUrl">
                    </td>
                    <td class="col-retUrl"></td>
                </tr><?
                $intCounter++;
            }
            ?>
        </table>
        <span>Время выполнения php скрипта: </span><span class="time-php"><?=round(microtime(true) - $time, 2)?></span><br/>
        <span>Время выполнения js скрипта: </span><span class="time-js"></span>
        <?
//        $time = microtime(true) - $time;
        //// Ищем совпадения с нашим списком
        //if (isset($codes[$http_code])) {
        //    echo 'Сайт вернул ответ: '.$http_code.' - '.$codes[$http_code].'<br />';
        //    preg_match_all("/HTTP\/1\.[1|0]\s(\d{3})/",$data,$matches);
        //    array_pop($matches[1]);
        //    if (count($matches[1]) > 0) {
        //        // Идем дальше по списку, чтобы посмотреть, какие мы еще статус коды получили
        //        foreach ($matches[1] as $c) {
        //            echo $c.' - '.$codes[$c].'<br />';
        //        }
        //    }
        //    // Проверяем если урл поменялся или нет
        //    if ($toCheckURL != $new_url) {
        //        echo 'Новый URL: '.$new_url;
        //    }
        //}?>
    </body>
</html>
