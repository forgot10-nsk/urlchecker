/**
 * Created by frgt10 on 01.11.14.
 */
var setProgress = function(progress)
{
    $('.progressBar .progress').width(progress + '%');
    $('.progressBar .progressLabel').text(progress + '%');

};

var setCodeClass = function(lineNumber, code, retUrl)
{
    var $line = $('.line'+(lineNumber+1));
    var $newCodeCell = $line.find('.col-newCode');
    var $newUrlCell = $line.find('.col-newUrl');
    var $retUrlCell = $line.find('.col-retUrl');
    $newCodeCell.text(code);
    strRegTest = /^(.+)\.(jpg|png|gif|js|css)$/;
    if( (strRegTest.test(retUrl)) )
        code = 0;
    $line.addClass('code-'+ code);
    $retUrlCell.text(retUrl);
    strRegTest = /^(.*)\.ru(\/?)$/;
    retUrlClass = 'retUrlNotSame';
    if($newUrlCell.text().replace('www.', '') == retUrl.replace('www.', ''))
    {
        retUrlClass = 'retUrlSame';
    }
    else if( !(strRegTest.test(retUrl)) )
    {
        retUrlClass = 'retUrlSame';
    }
    $retUrlCell.addClass( retUrlClass );
};

var removeCodeClass = function(lineNumber)
{
    var $line = $('.line'+(lineNumber+1));
    oldattr = $line.attr('class');
    newattr = oldattr.replace(/code-\d+/, '');
    $line.attr('class', newattr);
    $line.find('.col-retUrl').removeClass('retUrlSame').removeClass('retUrlNotSame');
};

function checkURL(arURLs, onEnd, single, i, timeBegin)
{
    i = i || 0;
    single = single || false;
    urlCount = arURLs.length;
    timeBegin = timeBegin || Date.now();
    if(i >= urlCount)
    {
        var time =(Date.now() - timeBegin) / 1000.0;
        onEnd(time);
        return;
    }
    var url = arURLs[i]['new']['url'];
    if (undefined != url)
        $.get(
            '/check.php',
            {
                url: url
            },
            function(answ)
            {
                setCodeClass(i, ( Math.floor(arURLs[i]['old']['code'] / 100) ) == 4 ? 0 : answ.http_code, answ.newUrl);
               if(!single)
                {
                    setProgress(Math.round((i+1) * 100.0 / urlCount));
                    checkURL(arURLs, onEnd, false, ++i, timeBegin);
                }
            },
            'json'
        );
}

$(function()
{
    arURLs = self.arURLs;
    arCodes = self.arCodes;
    $('.close').click(function()
    {
        $(this).parent().hide();
        return false;
    });
    $('#btnCheck').on('click', function()
    {
        setProgress(0);
        checkURL(arURLs, function(time){$('.time-js').text(time);}, false );
        return false;
    });
    $('#btnCheck2').on('click', function()
    {
        setProgress(0);
        $('.btnRefresh').click();
        return false;
    });
    $('#btnCount').on('click', function()
    {
        console.log('left: ' + $(this).left);

        $('.popup')
            .prop('left', $(this).left)
            .prop('top', $(this).top + $(this).height)
            .show();

        var $arLines = $('.resultTable tr');
        var arRes = arCodes;
        //console.log(arRes);
        for (key in arRes) {
            arRes[key] = 0;
        }
        //console.log(arRes);
        $arLines.each(function(index){
            lineClass = $(this).attr('class');
            strRegTest = /(.*)code-(\d+)(.*)/;
            if( strRegTest.test(lineClass) )
            {
                var code = lineClass.replace(/(.*)code-(\d+)(.*)/, '$2');
                //console.log(code);
                arRes[code]++;
            }
        });
        //console.log(arRes);
        for (key in arRes) {
            if (arRes[key])
            {
                console.log('код '+key+': '+arRes[key]+' раз');
            }
        }
        return false;
    });

    $('.btnRefresh').on('click', function()
    {
        var line = $(this).closest("tr").attr("class");
        line = line.replace(/line(\d+)(.*)/, '$1') - 1;
        removeCodeClass(line);
        checkURL(arURLs, function(time){$('.time-js').text(time);}, true, line, 0 );
    });
    $('.btnObj').on('click', function()
    {
        var line = $(this).closest("tr").attr("class");
        //нужно найти класс line№ и оставить №
        line = line.replace(/line(\d+)(.)*/,"$1") - 1;
        var $obj = arURLs[line];
        console.log($obj);
    });
    $('.btnUrl').on('click', function()
    {
        //определить, old или new
        urlType = '';
        if($(this).closest("td").hasClass('old'))
            urlType = 'old';
        else if($(this).closest("td").hasClass('new'))
            urlType = 'new';
        if(urlType == '')
            return;
        //нужно найти класс line№ и оставить №
        arrayPos = Number($(this).closest("tr").attr("class").replace(/line(\d+)(.)*/,"$1") - 1);
        if(isNaN(arrayPos))
            return;
        //взять url
        strUrl = arURLs[arrayPos][urlType]['url'];
        //открыть в новой вкладке
        window.open(strUrl, '_blank');
    });
});