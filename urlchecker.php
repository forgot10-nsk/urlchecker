<?php
/**
 * Created by PhpStorm.
 * User: Awik.Ru
 * Date: 31.10.2014
 * Time: 19:42
 */
class UrlChecker
{
    protected $links = array();
    protected $urlPosition = -1;
    protected $codePosition = -1;
    private $arHttpCodes = array();

    public $temp = '';

    function __construct($csv, $oldDomainName, $newDomainName)
    {
        $this->arHttpCodes = include_once "codes.php";
        if (($handle = fopen($csv, "r")) == FALSE)
            return;
        $intCounter = 0;
        $maxlen = 3000;
        $data = fgetcsv($handle, $maxlen, ",");//name or type
        $data = fgetcsv($handle, $maxlen, ",");//table headers
        for($this->urlPosition = 0; $this->urlPosition < count($data); $this->urlPosition++)
            if($data[$this->urlPosition] == "Address")
                break;
        for($this->codePosition = 0; $this->codePosition < count($data); $this->codePosition++)
            if($data[$this->codePosition] == "Status Code")
                break;
        while (($data = fgetcsv($handle, $maxlen, ",")) !== FALSE)
        {
            $this->links[$intCounter]['old']['url'] = (!empty($data[$this->urlPosition])?$data[$this->urlPosition]:"");
            $this->links[$intCounter]['new']['url'] = str_replace($oldDomainName, $newDomainName, $this->links[$intCounter]['old']['url']);
            $this->links[$intCounter]['old']['code'] = (!empty($data[$this->codePosition])?$data[$this->codePosition]:"0");
            $this->links[$intCounter]['new']['code'] = '';
            $intCounter++;
        }
        fclose($handle);
    }
    public function getLinks()
    {
        return $this->links;
    }
    public function getUrlPosition()
    {
        return $this->urlPosition;
    }
    public function getCodePosition()
    {
        return $this->codePosition;
    }
    public function getCount()
    {
        return count($this->links);
    }
    public function getCodeDescription($code)
    {
        return empty($this->arHttpCodes[$code]) ? false : $this->arHttpCodes[$code];
    }
}